package com.pame.libros;

import com.pame.libros.controller.LibroControlador;
import com.pame.libros.model.Autor;
import com.pame.libros.model.Imagen;
import com.pame.libros.model.Libro;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(locations = {"classpath:jpaContext-tests.xml"})
public class LibrosApplicationTests {
    private ApplicationContext appContext;
    @BeforeEach
    public void init(){
        appContext = new ClassPathXmlApplicationContext("jpaContext-tests.xml");
    }

   /* @Test
    @DisplayName("Este es el de prueba")
    public void contextLoads() {
    assertEquals("1","1");
    }

*/
    @Test
    @DisplayName("En este test vamos a probar como es guardar un libro en la BBDD con imagenes y autores y luego leemos de la BBDD para comprobar que se hizo bien")
    void testCargaYDescargaDeLibrosConAutoresEImagenes() {


        Libro libro = new Libro();
        libro.setTitulo("Titulo de prueba");
        libro.setSoporte("Blando");
        libro.setISBN("45455454asasasasas");
        libro.setCiudad_publicacion("Caracas Venezuela");
        libro.setTematica("Horror");
        libro.setNumero_paginas(5896);
        libro.setIdioma("Español");
        libro.setObservaciones_sinopsis("Lorem ipsum dolor sit amet, " +
                "consectetuer adipiscing elit. Aenean commodo " +
                "ligula eget dolor. Aenean massa. Cum sociis " +
                "atoque penatibus et magnis dis parturient montes, " +
                "nascetur ridiculus mus. Donec quam felis, ultricies nec," +
                " pellentesque eu, pretium quis, sem. Nulla consequat massa " +
                "quis enim. Donec pede justo, fringilla vel, aliquet nec, " +
                "vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a," +
                " venenatis vitae, justo. Nullam dictum felis eu pede mollis " +
                "pretium. Integer tincidunt. Cras dapibus. Vivamus elementum " +
                "semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, " +
                "porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem " +
                "ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra " +
                "nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet." +
                " Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. " +
                "Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum " +
                "rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. " +
                "Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas " +
                "nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis " +
                "faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt." +
                " Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed " +
                "consequat, leo eget bibendum sodales, augue velit cursus nunc,");


        Imagen imagen1 = new Imagen();
        imagen1.setImagen_libro("aihvbaijsnv3489py5r9hispdnbpidsfhbpyt985qy46bnidsf78");

        Imagen imagen2 = new Imagen();
        imagen2.setImagen_libro("6165465165123641523654");

        Imagen imagen3 = new Imagen();
        imagen3.setImagen_libro("jgjglalalaldkkfipw4jodfgnoihgjoihonisj5er1hg65s1h35sr1th65srt1h6s5rt1h");



        Autor autor1 = new Autor();
        autor1.setNombre("Adrián Flores");
        autor1.setApellidos("Al contrario del pensamiento popular");
        autor1.setWeb("www.google.es");


        Autor autor2 = new Autor();
        autor2.setNombre("Adrián Flores");
        autor2.setApellidos("corto");
        autor2.setWeb("www.gyaaaa.es");

        libro.addAutor(autor1);
        libro.addAutor(autor2);

        libro.setImagen(imagen1);
        libro.setImagen(imagen2);
        libro.setImagen(imagen3);


        LibroControlador libroControlador = appContext.getBean(LibroControlador.class);
        libroControlador.addLibro(libro);

        Libro libroObtenido = libroControlador.obtenerLibro((long) 1);

        System.out.println(libroObtenido.getIdLibros() + " " + libroObtenido.getTitulo());
        //   assertEquals(libroControlador.addLibro(null), "NOT OK!");
    }



}
