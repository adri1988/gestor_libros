package com.pame.libros.utils;

import com.pame.libros.controller.AutorControlador;
import com.pame.libros.controller.EditorialControlador;
import com.pame.libros.controller.LibroControlador;
import com.pame.libros.model.Autor;
import com.pame.libros.model.Editorial;
import com.pame.libros.model.Imagen;
import com.pame.libros.model.Libro;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.pame.libros.utils.Utils.transformaDeJsonAObjetoAutor;
import static com.pame.libros.utils.Utils.transformaDeJsonAObjetoEditorial;

public class Gestor {

    private static ClassPathXmlApplicationContext appContext = null;

    public static ClassPathXmlApplicationContext getInstance() {

        if (appContext == null) {
            appContext = new ClassPathXmlApplicationContext("jpaContext.xml");
        }
        return appContext;
    }

    public static boolean existeAutor(String nombreAutor) {

        return getInstance().getBean(AutorControlador.class).existe(nombreAutor);
    }


    public static String aniadeAutor(String nombreAutor, String apellido, String web) {
        Autor autorNuevo = new Autor();
        autorNuevo.setNombre(nombreAutor);
        autorNuevo.setWeb(web);
        autorNuevo.setApellidos(apellido);

        AutorControlador autorControlador = getInstance().getBean(AutorControlador.class);

        return new Utils().TransformaAJson(autorControlador.addAutor(autorNuevo));
    }

    public static ArrayList<String> obtenerTodosLosLibrosEnJson() {


        return new Utils().TransformaAJson(getInstance().getBean(LibroControlador.class).obtenerTodosLosLibros());

    }

    public static void Init() {
        appContext = new ClassPathXmlApplicationContext("jpaContext.xml");
    }

    public static boolean existeLibro(String libroTitulo) {

        return getInstance().getBean(LibroControlador.class).existe(libroTitulo);
    }

    public static boolean existeEditorial(String editorial) {
        return getInstance().getBean(EditorialControlador.class).existe(editorial);
    }

    public static String aniadeEditorial(String editorial) {
        Editorial editorialNueva = getInstance().getBean(EditorialControlador.class).addEditorial(new Editorial(editorial));
        return new Utils().TransformaAJson(editorialNueva);
    }

    public static ArrayList<String> obtenerTodasLasEditorialesEnJson() {

        List<Editorial> editoriales = getInstance().getBean(EditorialControlador.class).obtenerTodasLasEditoriales();
        return new Utils().TransformaAJson((ArrayList<Editorial>) editoriales);


    }

    public static ArrayList<String> obtenerTodosLosAutoresEnJson() {

        List<Autor> autores = getInstance().getBean(AutorControlador.class).obtenerTodosLosAutores();
        return new Utils().TransformaAJson((ArrayList<Autor>) autores);


    }

    public static String aniadeLibro(Long idLibro, String libroTitulo,
                                     ArrayList<String> autoresPorID,
                                     String traductor,
                                     ArrayList<String> editorial,
                                     String ciudad,
                                     String pais,
                                     String anio,
                                     String ISBN,
                                     String idioma,
                                     String tematica,
                                     String soporte,
                                     String paginas,
                                     String numEjemplares,
                                     String ubicacionFisica,
                                     ArrayList<String> imagenes,
                                     String htmlText,
                                     String notas) {


        List<Autor> autores = autoresPorID.stream().map((String s) -> transformaDeJsonAObjetoAutor(s)).collect(Collectors.toList());
        List<Editorial> editorials = editorial.stream().map((String s) -> transformaDeJsonAObjetoEditorial(s)).collect(Collectors.toList());
        List<Imagen> imagens = imagenes.stream().map(imagen -> {
            return getImagen(imagen);
        }).collect(Collectors.toList());


        Integer paginasInt = null;
        if (paginas != null)
            paginasInt = Integer.parseInt(paginas);

        Integer numEjemplaresInt = null;
        if (numEjemplares != null)
            numEjemplaresInt = Integer.parseInt(numEjemplares);

        Integer anioInt = null;
        if (anio != null)
            anioInt = Integer.parseInt(anio);


        Libro libro = new Libro(idLibro, htmlText, notas, tematica, paginasInt, numEjemplaresInt, ubicacionFisica, ISBN, libroTitulo, idioma, soporte, ciudad, pais, anioInt, autores, imagens, editorials, traductor);

        getInstance().getBean(LibroControlador.class).addLibro(libro);

        return new Utils().TransformaAJson(libro);


    }

    @NotNull
    private static Imagen getImagen(String imagen) {
        Imagen img = new Imagen();
        img.setImagen_libro(imagen);
        return img;
    }

    public static String obtenerLibroEnJson(Integer idLibro) {
        return new Utils().TransformaAJson(getInstance().getBean(LibroControlador.class).obtenerLibro(idLibro.longValue()));
    }
}
