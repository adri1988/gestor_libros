package com.pame.libros.utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pame.libros.controller.AutorControlador;
import com.pame.libros.controller.EditorialControlador;
import com.pame.libros.model.Autor;
import com.pame.libros.model.Editorial;
import com.pame.libros.model.Imagen;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Utils {


    public static ArrayList<Imagen> getImagens(ArrayList<String> imagenes) {
        ArrayList<Imagen> img = null;
        img = new ArrayList<>();
        for (String imagen : imagenes) {
            Imagen imagenPame = new Imagen();
            imagenPame.setImagen_libro(imagen);
            img.add(imagenPame);
        }
        return img;
    }

    @Nullable
    public static Autor transformaDeJsonAObjetoAutor(String autorEnJson) {

        try {
            Map<String, Object> myMap = new HashMap<String, Object>();
            ObjectMapper objectMapper = new ObjectMapper();
            myMap = objectMapper.readValue(autorEnJson, HashMap.class);
            Integer id = (Integer) myMap.get("id");
            // String idS = (String) myMap.get("id");
            return Gestor.getInstance().getBean(AutorControlador.class).obtenerAutor(Long.valueOf(id));
        } catch (IOException e) {
            System.out.println("Error en transformaDeJsonAObjetoAutor con la entrada: " + autorEnJson);
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Editorial transformaDeJsonAObjetoEditorial(String editorialEnJson) {

        try {
            Map<String, Object> myMap = new HashMap<String, Object>();
            ObjectMapper objectMapper = new ObjectMapper();
            myMap = objectMapper.readValue(editorialEnJson, HashMap.class);
            Integer id = (Integer) myMap.get("idEditorial");
            return Gestor.getInstance().getBean(EditorialControlador.class).obtenerEditorial(id);
        } catch (IOException e) {
            System.out.println("Error en transformaDeJsonAObjetoEditorial con la entrada: " + editorialEnJson);
            e.printStackTrace();
            return null;
        }
    }

    public <T> String TransformaAJson(T elemento) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(elemento);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }

    public <T> ArrayList<String> TransformaAJson(ArrayList<T> lista) {

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<String> jsons = new ArrayList<String>();

        for (T elemento : lista) {

            try {
                jsons.add(mapper.writeValueAsString(elemento));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return jsons;
    }


}
