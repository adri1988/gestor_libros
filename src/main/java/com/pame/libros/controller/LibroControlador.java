package com.pame.libros.controller;

import com.pame.libros.model.Libro;
import com.pame.libros.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;


@Controller
public class LibroControlador {

    @Autowired
    private LibroService libroService;


    public Libro addLibro(Libro libro) {
        return libroService.save(libro);

    }

    public Libro obtenerLibro(String titulo) {
        return libroService.obtenerLibro(titulo);

    }


    public Libro obtenerLibro(Long idLibro){
            return libroService.obtenerLibro(idLibro);

    }

    public boolean existe(String libroTitulo) {
        return libroService.existe(libroTitulo);

    }

    public ArrayList<Libro> obtenerTodosLosLibros() {
        return libroService.obtenerTodosLosLibros();
    }
}
