package com.pame.libros.controller;


import com.pame.libros.model.Editorial;
import com.pame.libros.service.EditorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EditorialControlador {
    @Autowired
    EditorialService editorialService;


    public Editorial addEditorial(Editorial editorial) {
        return editorialService.save(editorial);
    }

    public Editorial obtenerEditorial(Integer id) {
        return editorialService.obtenerEditorial(id);
    }

    public boolean existe(String nombreEditorial) {
        return editorialService.existe(nombreEditorial);

    }

    public List<Editorial> obtenerTodasLasEditoriales() {
        return editorialService.obtenerTodasLasEditoriales();
    }
}
