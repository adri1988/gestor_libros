package com.pame.libros.controller;

import com.pame.libros.model.Autor;
import com.pame.libros.service.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AutorControlador {

    @Autowired
    private AutorService autorService;


    public Autor addAutor(Autor autor) {

        return autorService.save(autor);


    }

    public Autor obtenerAutor(Long id){
        return autorService.obtenerAutor(id);
    }

    public boolean existe(String nombreAutor) {

        boolean resultadoDeBusqueda = autorService.existe(nombreAutor);

        return resultadoDeBusqueda;

    }

    public List<Autor> obtenerTodosLosAutores() {
        return autorService.obtenerTodosLosAutores();
    }

}
