package com.pame.libros.repository;

import com.pame.libros.model.Autor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutorRepositorio extends JpaRepository<Autor,Long> {

    int countByNombreEquals(String nombre);
}
