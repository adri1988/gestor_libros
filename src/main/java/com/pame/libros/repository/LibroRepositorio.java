package com.pame.libros.repository;

import com.pame.libros.model.Libro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibroRepositorio extends JpaRepository<Libro,Long> {

    int countByTituloEquals(String titulo);

    Libro findByTituloEquals(String titulo);

    @Query(value = "SELECT * FROM libros", nativeQuery = true)
    List<Libro> findAll();

}
