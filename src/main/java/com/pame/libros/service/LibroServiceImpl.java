package com.pame.libros.service;

import com.pame.libros.model.Libro;
import com.pame.libros.repository.LibroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;


@Service("LibroService")
public class LibroServiceImpl implements LibroService{

    @Autowired
    private LibroRepositorio libroRepositorio;

    @Override
    public ArrayList<Libro> obtenerTodosLosLibros() {
        return (ArrayList<Libro>) libroRepositorio.findAll();
    }

    @Override
    public Libro obtenerLibro(String titulo) {
        return libroRepositorio.findByTituloEquals(titulo);
    }

    @Transactional
    public Libro save(Libro libro)  {

         return libroRepositorio.save(libro);
    }

    @Transactional
    public Libro obtenerLibro(Long idLibro) {

        return libroRepositorio.findOne(idLibro);
    }

    @Override
    public boolean existe(String libroTitulo) {

        return libroRepositorio.countByTituloEquals(libroTitulo) != 0;
    }
}
