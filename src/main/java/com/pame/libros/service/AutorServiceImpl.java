package com.pame.libros.service;

import com.pame.libros.model.Autor;
import com.pame.libros.repository.AutorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Service("ServicioAutor")
public class AutorServiceImpl implements AutorService {


    @Autowired
    private AutorRepositorio autorRepositorio;

    @Override
    public Autor obtenerAutor(Long id) {
        return autorRepositorio.findOne(id);
    }

    @Override
    public List<Autor> obtenerTodosLosAutores() {
        return autorRepositorio.findAll();
    }

    @Override
    public boolean existe(String nombreAutor) {
        Assert.notNull(nombreAutor, "El autor");
        return autorRepositorio.countByNombreEquals(nombreAutor) != 0;

    }

    @Transactional
    public Autor save(Autor autor) {
        Assert.hasText(autor.getNombre(), "Nombre no debe ser vacío");
        return autorRepositorio.saveAndFlush(autor);
    }
}
