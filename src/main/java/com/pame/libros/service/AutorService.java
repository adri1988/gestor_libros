package com.pame.libros.service;

import com.pame.libros.model.Autor;

import java.util.List;

public interface AutorService {

    Autor save(Autor autor);

    Autor obtenerAutor(Long id);

    boolean existe(String nombreAutor);

    List<Autor> obtenerTodosLosAutores();
}
