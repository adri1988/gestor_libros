package com.pame.libros.service;

import com.pame.libros.model.Editorial;

import java.util.List;

public interface EditorialService {

    Editorial save(Editorial autor);

    Editorial obtenerEditorial(Integer id);

    boolean existe(String nombreEditorial);

    List<Editorial> obtenerTodasLasEditoriales();
}
