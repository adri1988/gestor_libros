package com.pame.libros.service;

import com.pame.libros.model.Libro;

import java.util.ArrayList;

public interface LibroService {

    Libro save(Libro libro);

    Libro obtenerLibro(Long idLibro);

    boolean existe(String libroTitulo);

    Libro obtenerLibro(String titulo);

    ArrayList<Libro> obtenerTodosLosLibros();
}
