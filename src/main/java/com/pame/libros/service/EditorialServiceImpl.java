package com.pame.libros.service;

import com.pame.libros.model.Editorial;
import com.pame.libros.repository.EditorialRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;


@Service("EditorialService")
public class EditorialServiceImpl implements EditorialService {

    @Autowired
    EditorialRepositorio editorialRepositorio;


    @Override
    public Editorial save(Editorial editorial) {

        return editorialRepositorio.saveAndFlush(editorial);
    }

    @Override
    public Editorial obtenerEditorial(Integer id) {

        return editorialRepositorio.findOne(id);
    }

    @Override
    public boolean existe(String nombreEditorial) {

        Assert.hasText(nombreEditorial, "No debe ser vacío el nombre de editorial");
        ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("nombreEditorial", exact().ignoreCase());
        Example<Editorial> example = Example.of(new Editorial(nombreEditorial), matcher);
        return editorialRepositorio.count(example) > 0;

    }

    @Override
    public List<Editorial> obtenerTodasLasEditoriales() {
        return editorialRepositorio.findAll();
    }
}
