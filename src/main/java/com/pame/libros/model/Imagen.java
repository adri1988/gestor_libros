package com.pame.libros.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "imagenes")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idimagenes")
public class Imagen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idImagenes")
    private Long idimagenes;

    @Column(name = "imagen_libro", columnDefinition = "TEXT")
    private String imagen_libro;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Libro libro;

    public Long getIdimagenes() {
        return idimagenes;
    }

    public void setIdimagenes(Long idimagenes) {
        this.idimagenes = idimagenes;
    }

    public String getImagen_libro() {
        return imagen_libro;
    }

    public void setImagen_libro(String imagen_libro) {
        this.imagen_libro = imagen_libro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Imagen)) return false;

        Imagen imagen = (Imagen) o;

        if (idimagenes != null ? !idimagenes.equals(imagen.idimagenes) : imagen.idimagenes != null) return false;
        return imagen_libro != null ? imagen_libro.equals(imagen.imagen_libro) : imagen.imagen_libro == null;
    }

    @Override
    public String toString() {
        return "Imagen{" +
                "idimagenes=" + idimagenes +

                ", imagen_libro='" + imagen_libro + "}";
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }
}
