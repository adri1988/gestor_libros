package com.pame.libros.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "libros")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idLibros")
public class Libro {

    private Long idLibros;
    private String observaciones_sinopsis;
    private String notas;
    private String tematica;
    private Integer numero_paginas;
    private Integer numero_ejempares;
    private String ubicacionFisica;
    private String ISBN;
    private String titulo;
    private String idioma;
    private String soporte;
    private String ciudad_publicacion;
    private String pais_publicacion;
    private Integer anio_de_publicacion;
    private List<Autor> autores = new ArrayList<>();
    private List<Imagen> imagenes = new ArrayList<Imagen>();
    private List<Editorial> editoriales = new ArrayList<>();
    private String traductor;


    public Libro(String observaciones_sinopsis, String notas, String tematica, Integer numero_paginas, Integer numero_ejempares, String ubicacionFisica, String ISBN, String titulo, String idioma, String soporte, String ciudad_publicacion, String pais_publicacion, Integer anio_de_publicacion, List<Autor> autores, List<Imagen> imagenes, List<Editorial> editoriales, String traductor) {
        this.observaciones_sinopsis = observaciones_sinopsis;
        this.notas = notas;

        this.tematica = tematica;
        this.numero_paginas = numero_paginas;
        this.numero_ejempares = numero_ejempares;
        this.ubicacionFisica = ubicacionFisica;
        this.ISBN = ISBN;
        this.titulo = titulo;
        this.idioma = idioma;
        this.soporte = soporte;
        this.ciudad_publicacion = ciudad_publicacion;
        this.pais_publicacion = pais_publicacion;
        this.anio_de_publicacion = anio_de_publicacion;
        this.autores = autores;
        this.autores.forEach(autor -> {
                    autor.addLibro(this);
                }
        );
        this.imagenes = imagenes;
        this.imagenes.forEach(imagen -> {
            imagen.setLibro(this);
        });
        this.editoriales = editoriales;
        this.editoriales.forEach(lEditorial -> {
            lEditorial.setLibro(this);
        });
        this.traductor = traductor;
    }

    public Libro(Long idLibros, String observaciones_sinopsis, String notas, String tematica, Integer numero_paginas, Integer numero_ejempares, String ubicacionFisica, String ISBN, String titulo, String idioma, String soporte, String ciudad_publicacion, String pais_publicacion, Integer anio_de_publicacion, List<Autor> autores, List<Imagen> imagenes, List<Editorial> editoriales, String traductor) {

        this.idLibros = idLibros;
        this.observaciones_sinopsis = observaciones_sinopsis;
        this.notas = notas;

        this.tematica = tematica;
        this.numero_paginas = numero_paginas;
        this.numero_ejempares = numero_ejempares;
        this.ubicacionFisica = ubicacionFisica;
        this.ISBN = ISBN;
        this.titulo = titulo;
        this.idioma = idioma;
        this.soporte = soporte;
        this.ciudad_publicacion = ciudad_publicacion;
        this.pais_publicacion = pais_publicacion;
        this.anio_de_publicacion = anio_de_publicacion;
        this.autores = autores;
        this.autores.forEach(autor -> {
                    autor.addLibro(this);
                }
        );
        this.imagenes = imagenes;
        this.imagenes.forEach(imagen -> {
            imagen.setLibro(this);
        });
        this.editoriales = editoriales;
        this.editoriales.forEach(lEditorial -> {
            lEditorial.setLibro(this);
        });
        this.traductor = traductor;
    }

    @Column(name = "traductor")
    public String getTraductor() {
        return traductor;
    }

    public Libro() {
    }

    public void setTraductor(String traductor) {
        this.traductor = traductor;
    }

    @Column(name = "notas", columnDefinition = "TEXT")
    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Column(name = "numero_ejempares")
    public Integer getNumero_ejempares() {
        return numero_ejempares;
    }

    public void setNumero_ejempares(Integer numero_ejempares) {
        this.numero_ejempares = numero_ejempares;
    }

    @Column(name = "ubicacionFisica")
    public String getUbicacionFisica() {
        return ubicacionFisica;
    }

    public void setUbicacionFisica(String ubicacionFisica) {
        this.ubicacionFisica = ubicacionFisica;
    }

    @Column(name = "pais_publicacion")
    public String getPais_publicacion() {
        return pais_publicacion;
    }

    public void setPais_publicacion(String pais_publicacion) {
        this.pais_publicacion = pais_publicacion;
    }

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "editorial_libro",
            joinColumns = {@JoinColumn(name = "idLibros", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "idEditorial", nullable = false, updatable = false)}
    )

    public List<Editorial> getEditoriales() {
        return editoriales;
    }

    public void setEditoriales(List<Editorial> editoriales) {
        this.editoriales = editoriales;
    }

    @Override
    public String toString() {

        return "Libro{" +
                "idLibros=" + idLibros +
                ", observaciones_sinopsis='" + observaciones_sinopsis + '\'' +
                ", tematica='" + tematica + '\'' +
                ", numero_paginas=" + numero_paginas +
                ", ISBN='" + ISBN + '\'' +
                ", titulo='" + titulo + '\'' +
                ", idioma='" + idioma + '\'' +
                ", soporte='" + soporte + '\'' +
                ", lugar_de_publicacion='" + ciudad_publicacion + '\'' +
                ", anio_de_publicacion=" + anio_de_publicacion +
                ", autores=[" + obtenerNombresAutores() + " ]" +
                ", editoriales=[" + obtenerNombresEditorial() + " ]" +
                '}';
    }

    @NotNull
    private String obtenerNombresAutores() {
        String autoresNombres = "";
        for (Autor autor : autores) {
            autoresNombres = autoresNombres + "  " + autor.getNombre();
        }
        return autoresNombres;
    }

    @NotNull
    private String obtenerNombresEditorial() {
        String editorialesNombres = "";
        for (Editorial ditorial : editoriales) {
            editorialesNombres = editorialesNombres + "  " + ditorial.getNombreEditorial();
        }
        return editorialesNombres;
    }

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "autor_libro",
            joinColumns = {@JoinColumn(name = "idLibro", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "idAUtor", nullable = false, updatable = false)}
    )
    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idLibro", nullable = false)
    public Long getIdLibros() {

        return idLibros;
    }

    public void setIdLibros(Long idLibros) {
        this.idLibros = idLibros;
    }


    @Column(name = "observaciones_sinopsis" ,columnDefinition="TEXT")
    public String getObservaciones_sinopsis() {
        return observaciones_sinopsis;
    }

    public void setObservaciones_sinopsis(String observaciones_sinopsis) {
        this.observaciones_sinopsis = observaciones_sinopsis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Libro)) return false;

        Libro libro = (Libro) o;


        return getIdLibros() != null ? getIdLibros().equals(libro.getIdLibros()) : libro.getIdLibros() == null;
    }


    @Column(name = "tematica")
    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    @Column(name = "numero_paginas")
    public Integer getNumero_paginas() {
        return numero_paginas;
    }

    public void setNumero_paginas(Integer numero_paginas) {
        this.numero_paginas = numero_paginas;
    }

    @Column(name = "ISBN")
    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
    @Column(name = "titulo")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Column(name = "idioma")
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Column(name = "soporte")
    public String getSoporte() {
        return soporte;
    }

    public void setSoporte(String soporte) {
        this.soporte = soporte;
    }

    @Column(name = "lugar_de_publicacion")
    public String getCiudad_publicacion() {
        return ciudad_publicacion;
    }

    public void setCiudad_publicacion(String lugar_de_publicacion) {
        this.ciudad_publicacion = lugar_de_publicacion;
    }

    @Column(name = "anio_de_publicacion")
    public Integer getAnio_de_publicacion() {
        return anio_de_publicacion;
    }

    public void setAnio_de_publicacion(Integer anio_de_publicacion) {
        this.anio_de_publicacion = anio_de_publicacion;
    }


    public void addAutor(Autor autor) {
        autores.add(autor);
        if (!autor.getLibros().contains(this))
            autor.addLibro(this);
    }

    public void addEditorial(Editorial editorial) {
        editoriales.add(editorial);
        if (!editorial.getLibros().contains(this))
            editorial.setLibro(this);
    }

    @JsonManagedReference
    @OneToMany(mappedBy = "libro", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Imagen> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<Imagen> imagenes) {
        this.imagenes = imagenes;
    }

    public void setImagen(Imagen imagen) {
        imagenes.add(imagen);
        imagen.setLibro(this);
    }
}
