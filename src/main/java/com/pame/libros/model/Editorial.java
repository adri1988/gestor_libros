package com.pame.libros.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "editoriales")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idEditorial")
public class Editorial {


    private Integer idEditorial;
    private String nombreEditorial;
    private List<Libro> libros = new ArrayList<>();

    public Editorial() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEditorial", nullable = false)
    public Integer getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    public Editorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }

    @Column(name = "nombre_editorial")
    public String getNombreEditorial() {
        return nombreEditorial;
    }

    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "editoriales")
    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }

    @Override
    public String toString() {

        String librosNombres = "";
        for (Libro libroL : libros) {
            librosNombres = librosNombres + "  " + libroL.getTitulo();
        }
        return "Editorial{" +
                "idEditorial=" + idEditorial +
                ", nombreEditorial='" + nombreEditorial + '\'' +
                ", libros=[" + librosNombres + "]" +
                '}';
    }

    public void setLibro(Libro libro) {
        libros.add(libro);
        if (!libro.getEditoriales().contains(this))
            libro.addEditorial(this);
    }
}
