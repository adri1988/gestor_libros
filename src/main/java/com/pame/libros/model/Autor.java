package com.pame.libros.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "autores")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idAutor")
public class Autor {


    List<Libro> libros = new ArrayList<>();

    private Long idAutor;


    private String nombre;

    private String web;

    private String apellidos;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Autor)) return false;

        Autor autor = (Autor) o;

        if (getIdAutor() != null ? !getIdAutor().equals(autor.getIdAutor()) : autor.getIdAutor() != null) return false;
        if (getNombre() != null ? !getNombre().equals(autor.getNombre()) : autor.getNombre() != null) return false;
        if (getWeb() != null ? !getWeb().equals(autor.getWeb()) : autor.getWeb() != null) return false;
        return getApellidos() != null ? getApellidos().equals(autor.getApellidos()) : autor.getApellidos() == null;
    }

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "autores")
    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }

    public void addLibro(Libro libro){
        libros.add(libro);
        if (!libro.getAutores().contains(this))
            libro.addAutor(this);
    }

    @Override
    public String toString() {
        return "Autor{" +
                "nombre='" + nombre + '\'' +
                ", web='" + web + '\'' +
                ", descripcion='" + apellidos + '\'' +
                ", Libros='" + libros + '\'' +
                '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAutor")
    public Long getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Long idAutor) {
        this.idAutor = idAutor;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "web")
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String descripcion) {
        this.apellidos = descripcion;
    }

}
